
T.actions = (_action, _target)->
	switch _action 
	
		when 'navigate' then T.navigate(_target)
		when 'initCamera' then T.initCamera(_target)
		when 'takePicture' then T.captureImage(_target)
		when 'picUpload' then T.picUpload(T._data)
		when 'getLatestPostShare' then T.getLatestPostShare(_target)
		when 'publishWelcome' then T.publishWelcome(_target)
		when 'prevPost' then T.likePosts.prev()
		when 'nextPost' then T.likePosts.next()
		when 'getLikePosts' then T.get5LikePosts(_target)
		when 'getTweetList' then T.getTweetList(_target)
		when 'populateMP3' then T.Music.Populate(_target)
		when 'getTweet' then T.getTweet()
		when 'getGraph' then T.getGraph(_action, _target)
		when 'connectlinkedin' then T.getConnect(_target)
		when 'getMatch' then T.getMatch(_action, _target)
		when 'getFeedback' then T.getFeedback(_action, _target)
		when 'getHoroscope' then T.getHoroscope(_action, _target)
		when 'like_this' then T.like_this(_target)
		when 'share_this' then T.share_this(_target)
		when 'tweet_this' then T.tweet_this(_target)
		else console.log "Unknown Action"

	null 

T.publishWelcome = (_target)->
	_url = "http://tagbin.in/phpHandler/status_post.php"
	$('.btn.blue').removeClass 'pulsed'
	T.actionlist.addStack 'Posting...', '#33B5E5', 4
	$.post _url, {tagID: T.serial.tagID}, (response)->
		$.post 'http://tagbin.in/phpHandler/store/store.php', {ID:response, type:'checkin'}, (r) ->
			console.log r
		console.log response
		T.actionlist.addStack('Successful...', '#33B5E5', 4)
		T.getPoint 'statuspost', T.serial.tagID
		null
	null

T.speak = (say, rate)->
	tts = chrome.tts
	if !rate
		rate = 0.8
	tts.speak(say, {'lang': 'en-US', 'rate': rate});