###
Notifications Module
Left Side, Below User-Picture
###

T.Notif = 

	# To store how many have been served
	Base: ['0']

	Create: ( message, type , callback)->
		target = $("ul.notif.actions")
		element = $("<li/>").addClass('action')

		# Apply Options
		element.text message
		element.addClass type

		# Append to list
		target.append element
		
		# Return Callback, if present
		callback() if typeof callback is 'function'

	isSuccess: ()->

	isError: ()->

	isInfo: ()->


