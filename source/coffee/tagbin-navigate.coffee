T.navigate = (_target)->
	$.ajax({
		url: _target
		cache: false,
		success: (_res)->
		
			$(".app-content").hide "slide",{direction: 'down'}, 300, ()->
				$(".app-content").html _res
				$(".app-content").show "slide", {direction: 'up'}, 300

		
				# Full height of .box.app container
				$('.box.app').css('height', (document.height-50).toString() + 'px')

				console.log "Searching for .loadActions"
				
				# Search for actions available on load
				# console.log $(".loadActions").length
				$(".loadActions").each (_index)->
					T.actions $(@).attr('data-action'), $(@).attr('data-target')
					null
				null

			null

		error: (_res)->
			console.log "Error results"
			null

	}).done (_res)->
		console.log "Navigation Done, Applying View..."
		switch _target
			when 'like-post.html' then T.updateView 'entypo-thumbs-up', 'Like Posts', T.Utils.Colors.Purple
			when 'tagplay.html' then T.updateView 'icomoon-music', 'Tag Play', T.Utils.Colors.Orange
			when 'share.html' then T.updateView 'entypo-export', 'Share Posts', T.Utils.Colors.Red
			when 'status-post.html' then T.updateView 'entypo-vcard', 'Status Post', T.Utils.Colors.Blue
			when 'pic-upload.html' then T.updateView 'icomoon-image', 'Pic Upload', T.Utils.Colors.Green
			when 'feedback.html' then T.updateView 'icomoon-feed-2', 'Feedback', T.Utils.Colors.Brown
			when 'fortune.html' then T.updateView 'icomoon-dice', 'Horoscope', T.Utils.Colors.Vodared
			when 'leaderboard.html' then T.updateView 'icomoon-dashboard', 'LeaderBoard', T.Utils.Colors.Yellow
			when 'match_finder.html' then T.updateView 'icomoon-heart', 'FriendFinder', T.Utils.Colors.Pink
			when 'twitter-retweet.html' then T.updateView 'icomoon-users', 'Retweet', T.Utils.Colors.Dark_Green
			when 'connect.html' then T.updateView 'icomoon-connection', 'Connect', T.Utils.Colors.Color1
			else 'No Nav Switches'
		
		null
		

T.navActions = (_action)->
	if _action is 'navLikePost'
		T.actionlist.replace(['Swipe Tag', 'Like Post'])
		console.log "done-navlikepost"
	else console.log "Default Nav Action"
	null


T.updateView = (_iconclass, _viewname, _color)->
	T.Utils.CurrentColor = _color
	$(".main-page-title i")[0].className = "pageicon " + _iconclass
	$(".main-page-title span.name").text _viewname
	$(".main-page-title").show "drop", {direction: "left"}, 300
	$(".main-page-title").css 'color', _color
	$(".main-userpic-wrap .username").css 'background-color', _color
	$(".main-userpic-wrap .username").css 'border-bottom-color', _color