
# Ninja Handlers here
$(T.ninja).on 'sendPhoto', (e)->
	_url = "http://tagbin.in/phpHandler/fb_post.php"
	_urlTemp = "http://tagbinphp-temp.herokuapp.com/phpHandler/fb_post.php"

	$.post _url, {
		imgdata: T.readyImageData
		tagID: T.serial.tagID
	}, (response)->
			$.post 'http://tagbin.in/phpHandler/store/store.php', {ID:response, type:'photo'}, (r) ->
				console.log r
			$('#capture').removeClass 'pulsed'
			$('#capture .text1').text "Done"
			T.getPoint 'photoupload', T.serial.tagID
			T.actionlist.addStack('Uploaded...', 'green', 2)
		null
	null


$(T.ninja).on 'likePost', (e)->
	_url = "http://tagbin.in/phpHandler/fb_likepost.php"
	_options = 
		postID: $('.posts-list .active').attr('data-post-id')
		tagID: T.serial.tagID
	$.post _url, _options, (_response)->
		console.log _response
		if _response.slice(_response.length-5, _response.length) != 'likes'
			T.actionlist.addStack("You're tagID is not registered or expired", 'purple', 1)
		else
			if _options.postID in T.likePosts.liked
				T.actionlist.addStack("You've already liked", 'purple', 1)
			else
				T.likePosts.liked.push _options.postID
				$('.posts-list .active .count').text(parseInt($('.posts-list .active .count').text())+1)
				T.actionlist.addStack('Liked...', 'purple', 1)
				T.getPoint 'like', T.serial.tagID
		null
	null


$(T.ninja).on 'sharePost', (e)->
	_url = "http://tagbin.in/phpHandler/share.php"
	_options = 
		postID: $('.posts-list .active').attr('data-post-id')
		tagID: T.serial.tagID
	$.post _url, _options, (_response)->
		console.log _response
		T.actionlist.addStack('shared...', 'red', 3)
		T.getPoint 'statusshare', T.serial.tagID
		null
	null
