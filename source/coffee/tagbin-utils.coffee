
# Various utilities used by the app are defined here
T.Utils =

	# To Return random coordinates
	RandCoord: ()->
		_x = Math.floor Math.random()*document.width
		_y = Math.floor Math.random()*document.height
		return [parseInt(_x), parseInt(_y)]

	# Returns distance between two points
	# 		Points are defined as {x:0, y:0}
	Distance: (p1,p2)->
		dx = p2.x - p1.x
		dy = p2.y - p1.y
		return Math.sqrt(dx*dx + dy*dy)

	# Retuns String with First Letter UpperCased
	toStringFU: (string)->
		return string[0].toUpperCase() + string.substr(1)

	# Returns a post object based on data
	NewPost: (_data)->

		### Structure of _data
			|-- likes
			|	-- count
			|	-- data[]
			|     -- id
			|     -- name
			|-- from 
			|	-- id 
			|	-- name
			|-- id
			|-- message

		###
		
		# End if post is not status
		return if _data.type is not 'status'

		# Proceed with values
		_data.likes = _data.likes ? {count:'0', data: null}
		try 
			likes_count = _data.likes.data.length
		catch err
			likes_count = 0
		likes_data = _data.likes.data ? null
		message = _data.message ? "No Message"
		if message.length > 240
			message = message.slice(0,240)+"..."
		from_name = _data.from.name ? "User"
		from_id = _data.from.id ? "No ID"
		post_id = _data.id 
		
		
		# Create Post Object
		_pobj = $("<div/>")
		_pobj.addClass("social-post row-fluid")
			.append( $("<div/>").addClass('span3')  )
			.append( $("<div/>").addClass('span9') )
			.attr 'data-post-id', post_id

		# Add inner fields
		_pobj.find('.span3')
			.append( $("<div/>").addClass('pic-whoposted').attr('data-user-id', from_id ) )

		# Add picture
		# console.log "Adding Picture: " + from_id
		
		_picurl = 'http://graph.facebook.com/'+from_id+'/picture?type=large'
		
		T.getXHR _picurl, "blob", (responseBlob)->
			_ourl = window.webkitURL.createObjectURL responseBlob
			_pobj.find('.pic-whoposted').css 'background-image', 'url("' + _ourl + '")'
		
		_pobj.find('.span9')
			.append( $("<div/>").addClass('who-posted').text(from_name) )
			.append( $("<div/>").addClass('post-text') )
			.append( $("<div/>").addClass('post-likes') )
		_pobj.find('.post-likes')
			.append(  $("<div/>").addClass('count').text( likes_count ) )
			.append( $("<div/>").addClass('text_like') )
			.addClass("runAction").attr('data-action', 'like_this')
#			.attr 'onclick', 'T.like_this(' + post_id + ')'
		_pobj.find('.post-likes .text_like').append( $("<i/>").addClass('entypo-thumbs-up white') )
		_pobj.find('.post-text')
			.append(  $("<p/>").text( T.Utils.toStringFU(message) ) )
		_pobj.addClass('hidden tiltup')

		# Add Pictures of users who liked the post
		_pobj.find('.span9').append( $("<ul/>").addClass("like-users") )
		
		_k = 0
		return _pobj if not likes_data
		loop
			_tt = likes_data[_k]
			_ll = $("<li/>")
					.addClass('post-likes-pics')
					.attr('data-id', _tt.id)
					.attr('data-name', _tt.name)

			_likepicurl = 'http://graph.facebook.com/'+_tt.id+'/picture'
			_pobj.find("ul.like-users").append( _ll )
			_k++
			break if _k is likes_data.length or _k>6

		# Attach image to every member
		$(".post-likes-pics").each (index)->
			id = $(@).attr('data-id')
			url = "http://graph.facebook.com/" + id + "/picture"
			
			T.getXHR url, "blob", (responseBlob)->
				link = window.webkitURL.createObjectURL responseBlob
				selector = ".post-likes-pics[data-id='" + id + "']"
				console.log selector, $(selector).length
				$(selector).css 'background-image', 'url("' + link + '")'
		
		return _pobj

	# Color Variables
	Colors:
		Blue: '#33B5E5'
		Green: '#33DC49'
		Red: '#FF4444'
		Pink: '#ED3BA6'
		Orange: '#FFBB33'
		Yellow: '#f4b400'
		Purple: '#9d69b4'
		Dark_Green : '#151'
		Color1 : '#BB2'
		Color2 : '#CF0'
		Brown : '#D30'
		Vodared : '#ED1C24'

	CurrentColor: null