T.actionlist = 

	target: null

	findTarget: ()->
		if $(".actionlist").length then T.actionlist.target = $(".actionlist")
		else console.log "No actionlist on this page"
	
	clear: ()->
		tt = T.actionlist.findTarget()
		tt.find('li').hide "slide", {direction: 'up'}, 400, ()->
			$(this).remove()

	addActions: (_actionsArray)->
		_jj = _actionsArray
		T.actionlist.findTarget() if not T.actionlist.target
		
		# Add actions to list
		for key in _jj
			T.actionlist.addOne _jj.indexOf(key), key

	addOne: (_color, _text, id)->
#		if _id == 0
#			_aa = $("<li/>").addClass('action done').attr 'data-actionid', _id.toString()
#		else if _id == 1
#			_aa = $("<li/>").addClass('action waiting').attr 'data-actionid', _id.toString()
#		else
		_aa = $("<li/>").addClass('action done').attr('style', 'background-color:'+ _color + ';').attr 'id', id
		_aa.append $("<i/>").addClass('icomoon-minus')
		_aa.append $("<span/>").addClass('name').text(_text)

		T.actionlist.findTarget() if not T.actionlist.target

		T.actionlist.target.append _aa

	replace: (_actionsArray)->
		T.actionlist.findTarget() if not T.actionlist.target
		T.actionlist.clear()
		T.actionlist.addActions(_actionsArray)

	addStack: (action, color, id) ->
		a = $('.actionlist')[0]
		aa = $('.actionlist').find '#'+id
		if aa.length
			console.log 'exit'
			aa.find('.name').text action
			return
		if a.childElementCount >= 3
			tt = a.children[0]
			tt.remove()
		T.actionlist.addOne color, action, id
		setTimeout ()->
			$('.actionlist #'+id).hide 'slide', {direction: "up"}, 400, ()->
				$(this).remove()
		,10000