T.getGraph = (_action, _target)->
    T.checkTag (_tagid) ->
        console.log T.serial.tagID
    a= null
    i = 0
    _results = []
    T.checkTag (_tagid) ->
        $.getJSON 'http://tagbin.in/my/ut/createleaderboard.php?tagid='+T.serial.tagID, (response)->
            a = response
            console.log a
            null
    
        .done ()->
                for i in [0..a[4][0].length-1]
                    _results.push({
                        name:a[4][0][i] ,
                        value:a[1][0][i]
                    });
                $("#chartContainer").dxChart({
    
                    dataSource: _results,
                    commonAxisSettings: {
                        grid: {
                            visible:false
                        },
                    },
                    valueAxis: {
                        label: {
                            visible:false
                        },
                    },
                    argumentAxis: {
                        label: {
                            visible:false
                        },
                        title:'Update Status or Upload Pics To Score Points On Leaderboard ',
                    },
                    legend: {
                        visible:false
                    },
                    commonSeriesSettings: {
                        type:'bar',
                        argumentField: 'name',
                    },
                    series: [{   
                        color: "#d1bff0",
                        label: {
                            isHtml:true,
                            visible: true,
                            backgroundColor:"#cc0063",
                            connector: {
                                visible: true
                            }
                            customizeText: ()->
                                if(parseInt(this.valueText)==a[3]||this.argumentText==a[2])
                                    return this.argumentText+": "+this.valueText+" points"
                        },
                        name:'leader',           
                        valueField: 'value'
                    }]
                    tooltip: {
                        enabled: true,
                        argumentFormat: 'MM/d',
                        customizeText: ()-> 
                            return this.argumentText + ' : ' + this.valueText+' points'
                        },  
                });
                null


T.getPoint = (_type,_tagid)->
    _url = "http://tagbin.in/my/ut/leaderboarddemo.php"
    $.post _url, {_type:_type,_tagid:_tagid},(res)->
        console.log res
        null
    null