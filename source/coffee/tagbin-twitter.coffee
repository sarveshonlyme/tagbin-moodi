S = {
	tweets : null,
	clicked_tweet_id : null,
	response : null,
	err : null,
}

T.getTweet = (_taget) ->
	_url = "http://tagbin.in/phpHandler/twitter/getDetail.php"
	params = {type: 'getLatestIdByHashTag', hash_tag: 'priyankachopra', count: 100}
	T.actionlist.addStack 'Click on Tweet to reTweet', '#151', 6
	$.post( _url, params, (response) ->
		a = S.tweets = JSON.parse(response)
		clone_under = $("#tw_detail")
		to_clone = $(".blog")
		i=0
		for tweet in a
			clone = to_clone.clone()
			c = clone_under.append clone
			_from = tweet.user.name + ' @ ' +tweet.user.screen_name
			clone.find('.tw_from').text _from
			clone.find('.tw_content').text tweet.text
			clone.find('#retw_count').text tweet.retweet_count
			$('a.blog').last().attr 'id', i
			$.post 'http://tagbin.in/phpHandler/getDetails.php', {type:'getPictureSrc', url : tweet.user.profile_image_url}, (url_)->
				_bb = T.b64toblob url_, 'image/jpg'
				_bburl = window.webkitURL.createObjectURL _bb
				console.log "-->" + _bburl
				clone.find('.tw_user_img').attr 'src', _bburl
			i++
		$("#tw_detail").find(".blog")[0].innerHTML = ""
		$('.box.main .footer').addClass('pulsed').text "Swipe your Tag!"
		T.checkTag (_tagid)->
			console.log _tagid
			$('a').click ()->
				console.log this.id
				S.clicked_tweet_id = S.tweets[this.id].id_str
				T.actionlist.addStack "ReTweeting...", '#151', 6
				$(T.ninja).trigger 'retweet'
				null
			null
		null
		).done () ->
			console.log 'sarveshh'
			null
		null


T.getTweetList = (_target) ->
	_TARGET = _target
	$(_TARGET).empty()
	_url = "http://tagbin.in/phpHandler/twitter/getDetail.php"
	params = {type: 'getLatestIdByHashTag', hash_tag: 'priyankachopra', count: 20}
	$.post _url, params, (response) ->
		$(_TARGET).addClass 'inProcess'
		i = 0
		a = S.tweets = JSON.parse response
		console.log a
		loop
			_obj = a[i]
			_li = T.NewTweet _obj
			_li.addClass('active') if a.indexOf(_obj) is parseInt('2')
			$(_TARGET).append _li
			i++
			break if i is a.length
		$(_TARGET).removeClass 'inProcess'
		T.likePosts.next()
		T.likePosts.prev()
		T.likePosts.prev()
		T.likePosts.prev()

T.NewTweet = (_data) ->
	tweet = _data.text ? "No Tweet"
	from_name = _data.user.name + ' @ ' +_data.user.screen_name
	tweet_id = _data.id_str

	_pobj = $("<div/>")
	_pobj.addClass('social-post row-fluid')
		.append( $("<div/>").addClass('span3'))
		.append( $("<div/>").addClass('span9'))
		.attr 'data-post-id', tweet_id

	_pobj.find('.span3')
		.append $('<div/>').addClass('pic-whoposted').attr('data-user-id', tweet_id)

	_picurl = _data.user.profile_image_url

	T.getXHR _picurl, 'blob', (responseBlob) ->
		_ourl = window.webkitURL.createObjectURL responseBlob
		_pobj.find('.pic-whoposted').css 'background-image', 'url("'+_ourl+'")'
	_pobj.find('.span9')
		.append( $('<div/>').addClass('who-posted').text(from_name) )
		.append( $('<div/>').addClass('post-text') )
	_pobj.find('.post-text')
		.append $('<p/>').text(tweet)
	_pobj.addClass('hidden tiltup')
	return _pobj

T.tweet_this = (_action) ->
	T.actionlist.addStack 'Processing...', '#151', 6
	$(T.ninja).trigger 'retweet'

$(T.ninja).on 'retweet', (e)->
	console.log 'triggered'
	_url = "http://tagbin.in/phpHandler/twitter/getDetail.php"
	params = {type: 'reTweet', tagID : T.serial.tagID, tweetID:$('.social-post.active').attr('data-post-id')}
	$.post _url, params, (response) ->
		console.log response
		S.err = JSON.parse(response).errors
		if S.err then T.actionlist.addStack 'Already Tweeted', '', 6
		else T.actionlist.addStack "reTweeted", '', 6
	null

$(T.ninja).on 'getUserPicture', (e)->
	console.log 'pic-triggered'
	url = "http://tagbin.in/phpHandler/twitter/getDetail.php"
	$.post url, {type: 'getUserPicture'}, (_res)->
		_bb = T.b64toblob _res, 'image/jpg'
		_bburl = window.webkitURL.createObjectURL _bb
		console.log _bburl
