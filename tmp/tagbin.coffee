
# Main Namespace for tagbin functions
T =
	testData: {}
	user: {}
	init: null 			# The initialization function

	notify: null		# Notification module
	picReady: 0
	cI: null
	socket: null
	ninja: document.createElement 'div'

	useOffline: false
	Utils: {}

T.init = ()->
	
	### Check facebook login
	if not FB.getUserID()
		FB.login (_response)->
			console.log "Signed into facebook."
			console.log _response
		, {scope: 'publish_stream'}
	###

	# Full height of .box.app container
	$('.box.app').css('height', (document.height-50).toString() + 'px')

	# T.canvas_sea()

	# Initalize C Sockets
	# T.sockets()

	# Return null
	null

T.handleLoading = ()->

	$(".tb-loader").hide()

	# If .loader is present, set .overlay display: block
	if $(".loading").length then $(".overlay").css('display', 'block')

	$(".tb-loader").show "drop", {direction: "up"}, 500, ()->
		#T.canvasAnim()
		#$(".loading canvas").show()
		null

	window.setTimeout ()->
		$(".tb-loader .anim").removeClass 'anim'
		$(".tb-loader").hide "drop", {direction: 'down'}, 300, ()->
			###$(".loading").hide "fade", ()->
				$(".overlay").css 'display', 'none'
				window.location.href = './pic-upload.html'
			###
			# Show the menu after tagbin animation
			$("ul.loader-menu").show "drop",{direction: "down"}, 400
			$(".swipe_notification").show "drop", {direction: "down"}, 400
			T.checkTag (tagid)->
				console.log T.serial.tagID
			console.log "Loading Handled"

	,4000
	null

T.canvasAnim = (_params)->
	#window.clearInterval T.cI
	osfn = _params ? [Math.sin,Math.cos]
	canvas = $(".ldcanvas")[0]
	ctx = canvas.getContext '2d'
	ctx.fillStyle = '#555'
	ctx.strokeStyle = '#555'
	draw = (params)->
		canvas.width = document.width + 2000
		canvas.height = document.height 
		ctx.clearRect 0,0,canvas.width, canvas.height
		x = y = 0
		loop
			xVal = Math.PI/180*(x%360)
			yVal = Math.round ((canvas.height/2) + (Math.sin(xVal)*50)) 

			xVal2 = Math.PI/180*(x%360)
			yVal2 = Math.round ((canvas.height/2) + (Math.cos(xVal)*60)) 

			xVal3 = Math.PI/180*(x%360)
			yVal3 = Math.round ((canvas.height/2) + (Math.sin(xVal+9)*40)) 

			ctx.fillRect(x, yVal, 1,1)
			ctx.fillRect(x, yVal2, 1,1)
			ctx.fillRect(x, yVal3, 1,1)
			#console.log( [xVal,yVal] , [xVal2,yVal2], [xVal3,yVal3] )
			x = x+4
			break if x >= canvas.width
		ctx.save()
		null
	draw(osfn)
	#T.cI = window.setInterval draw(osfn), 3000
	null

T.reset = ()->
	$(".box.main .footer")
		.removeClass("animated")
		.removeClass("needy")
		.text("")
	




T.actionlist = 

	target: null

	findTarget: ()->
		if $(".actionlist").length then T.actionlist.target = $(".actionlist")
		else console.log "No actionlist on this page"
	
	clear: ()->
		tt = T.actionlist.findTarget()
		tt.find('li').hide "slide", {direction: 'up'}, 400, ()->
			$(this).remove()

	addActions: (_actionsArray)->
		_jj = _actionsArray
		T.actionlist.findTarget() if not T.actionlist.target
		
		# Add actions to list
		for key in _jj
			T.actionlist.addOne _jj.indexOf(key), key

	addOne: (_color, _text, id)->
#		if _id == 0
#			_aa = $("<li/>").addClass('action done').attr 'data-actionid', _id.toString()
#		else if _id == 1
#			_aa = $("<li/>").addClass('action waiting').attr 'data-actionid', _id.toString()
#		else
		_aa = $("<li/>").addClass('action done').attr('style', 'background-color:'+ _color + ';').attr 'id', id
		_aa.append $("<i/>").addClass('icomoon-minus')
		_aa.append $("<span/>").addClass('name').text(_text)

		T.actionlist.findTarget() if not T.actionlist.target

		T.actionlist.target.append _aa

	replace: (_actionsArray)->
		T.actionlist.findTarget() if not T.actionlist.target
		T.actionlist.clear()
		T.actionlist.addActions(_actionsArray)

	addStack: (action, color, id) ->
		a = $('.actionlist')[0]
		aa = $('.actionlist').find '#'+id
		if aa.length
			console.log 'exit'
			aa.find('.name').text action
			return
		if a.childElementCount >= 3
			tt = a.children[0]
			tt.remove()
		T.actionlist.addOne color, action, id
		setTimeout ()->
			$('.actionlist #'+id).hide 'slide', {direction: "up"}, 400, ()->
				$(this).remove()
		,10000





T.actions = (_action, _target)->
	switch _action 
	
		when 'navigate' then T.navigate(_target)
		when 'initCamera' then T.initCamera(_target)
		when 'takePicture' then T.captureImage(_target)
		when 'picUpload' then T.picUpload(T._data)
		when 'getLatestPostShare' then T.getLatestPostShare(_target)
		when 'publishWelcome' then T.publishWelcome(_target)
		when 'prevPost' then T.likePosts.prev()
		when 'nextPost' then T.likePosts.next()
		when 'getLikePosts' then T.get5LikePosts(_target)
		when 'getTweetList' then T.getTweetList(_target)
		when 'populateMP3' then T.Music.Populate(_target)
		when 'getTweet' then T.getTweet()
		when 'getGraph' then T.getGraph(_action, _target)
		when 'connectlinkedin' then T.getConnect(_target)
		when 'getMatch' then T.getMatch(_action, _target)
		when 'getFeedback' then T.getFeedback(_action, _target)
		when 'getHoroscope' then T.getHoroscope(_action, _target)
		when 'like_this' then T.like_this(_target)
		when 'share_this' then T.share_this(_target)
		when 'tweet_this' then T.tweet_this(_target)
		else console.log "Unknown Action"

	null 

T.publishWelcome = (_target)->
	_url = "http://tagbin.in/phpHandler/status_post.php"
	$('.btn.blue').removeClass 'pulsed'
	T.actionlist.addStack 'Posting...', '#33B5E5', 4
	$.post _url, {tagID: T.serial.tagID}, (response)->
		$.post 'http://tagbin.in/phpHandler/store/store.php', {ID:response, type:'checkin'}, (r) ->
			console.log r
		console.log response
		T.actionlist.addStack('Successful...', '#33B5E5', 4)
		T.getPoint 'statuspost', T.serial.tagID
		null
	null

T.speak = (say, rate)->
	tts = chrome.tts
	if !rate
		rate = 0.8
	tts.speak(say, {'lang': 'en-US', 'rate': rate});





T.b64toblob = (b64Data, contentType, sliceSize)->
	contentType = contentType || ''
	sliceSize = sliceSize || 1024

	charCodeFromCharacter = (c)->
		return c.charCodeAt(0)

	byteCharacters = atob(b64Data)
	byteArrays = []

	offset = 0

	while offset < byteCharacters.length
		slice = byteCharacters.slice(offset, offset + sliceSize)
		byteNumbers = Array::map.call(slice, charCodeFromCharacter)
		byteArray = new Uint8Array(byteNumbers)
		byteArrays.push byteArray
		offset += sliceSize

	blob = new Blob(byteArrays, {type: contentType})
	return blob;

T.imgToB64 = (url, _callback)->
	cc = document.createElement 'canvas'
	ctx = cc.getContext '2d'
	img = document.createElement 'img'
	img.src = url
	cc.width = img.width 
	cc.height = img.height 
	ctx.drawImage img, 0, 0
	if _callback then _callback(cc.toDataURL())
	else return cc.toDataURL()


T.getXHR = (_url, _type, _callback)->
	if _callback
		type = _type ? "blob"
		xhr = new XMLHttpRequest()
		xhr.open 'GET', _url , true
		xhr.responseType = type
		xhr.onload = (e)->
			#_bloburl = window.webkitURL.createObjectURL this.response
			#_callback(_bloburl)
			_callback this.response
		xhr.send()
	else return console.log "This function requires a callback, Exiting..."
		





T.videoEl = null
T.webcamSource = null
T._data = null

T.initCamera = (_target)->

	navigator = navigator || window.navigator

	onSuccess = (stream)->
		if T.videEl
			console.log "Video Element Resume"
			T.videoEl.play()
			null
		else 
			video = $(_target)[0]
			video.autoplay = true
			T.webcamSource = window.URL.createObjectURL stream
			video.src = T.webcamSource
			video.play()
			T.videoEl = video
			null

	onError = ()->
		console.log "Failed to initialize camera"

	navigator.webkitGetUserMedia {video: true, audio: false}, onSuccess, onError

	console.log "Camera Initiated"

	window.setTimeout ()->
		console.log T.videoEl
		console.log "Now Resuming Play"
		T.videoEl.play() if T.videoEl
		null
	,1000	
	null


T.captureImage = (_target, _cb)->
	video = $(_target)[0]
	video.play()
	T.checkTag (_tagid) ->
		console.log T.serial.tadID
	# Show a countdown
	for k in [1,2,3,4,5]
		$(video).parent().find(".countdown").append $("<div/>").addClass("c"+k.toString()).text(k.toString())

	$(video).parent().find(".countdown").append $("<div/>").addClass 'flash'

	window.setTimeout ()->
		video.pause()
		canvas = document.createElement 'canvas'
		canvas.width = 828
		canvas.height =  620
		ctx = canvas.getContext '2d'
		imgframe = document.createElement('img')
		imgframe.src = "./static/img/frame.png"
		ctx.drawImage video, 0, 0, 828, 620
		ctx.drawImage imgframe, 0, 0, 828, 620
		_data = canvas.toDataURL()
		T._data = _data
		console.log "Generated data-uri for image"
		$('#capture').addClass("runAction pulsed").attr('data-action','picUpload').attr 'data-target',_target
		$('#capture .text1').text "Upload"
		T.actionlist.addStack 'Click on Upload', 'green', 2
		$('#recapture').removeClass 'hide'
#		T.picUpload _data
		null
	, 5100

	### Re-create the image on canvas and export base64 encoded image data
	###
	
	if _cb then _cb(_data)

	null










T.connect = (options, _cb)->
	
	_postURL = "http://tagbin.in/phpHandler/getDetails.php"


	$.ajax({
		url: _postURL,
		type: 'post',
		data: {
				type: options.type
				tagID: options.tagID
			},
		cache: false,
		success: (data)->
			console.log data
			_cb(data) if (_cb)

		error: (e)->
			console.log e
	}).done ()->
		console.log "Finished"




T.getData = (_tagID, _callback)->
	T.connect {tagID: _tagID}, (_response)->
		_callback(_response) if _callback
		console.log "Details Done!"




T.getFeedback = (_action, _target) ->
	T.checkTag (_tagid) ->
		console.log T.serial.tagID
		$('.btn.yellow').removeClass('pulsed')
		$("input:radio[name=radio]").click () ->
			T.actionlist.addStack "Submiting...", '#D30', 10
			feed = $(this).val()
			console.log feed
			$.post 'http://tagbin.in/phpHandler/feedback/add_feed.php', {clientID:'', tagID:T.serial.tagID, rate: feed}, (res) ->
				console.log res
				T.actionlist.addStack "Feedback " + res, '', 10
				null
			null
		null






T.gdURL = "http://tagbin.in/phpHandler/getDetails.php"

T.latestPostData = null
T.id_to_like = null

T.getLatestPost = (_target)->
	console.log "Fetching latest post"
	_url = "http://tagbin.in/phpHandler/getDetails.php"
	_ops = {type: 'getLatestPost'}
	
	$.post _url, _ops, (response)->
	
		console.log JSON.parse(response)
		window.RESPONSE1 = JSON.parse(response).data[0]
		T.latestPostData = window.RESPONSE1
		
		_message = T.latestPostData.message
		_id = T.latestPostData.from.id
		_from = T.latestPostData.from.name
		_likesCount = T.latestPostData.likes.count

		$("#latestpost .who-posted").text _from
		$("#latestpost .post-text p").text _message
		$("#latestpost .post-likes .count").text _likesCount.toString()
		
		$.post T.gdURL, {id: _id, type: 'getUserPicture'}, (_res)->
	
			_bb = T.b64toblob _res, 'image/jpg'
			_bburl = window.webkitURL.createObjectURL _bb
			$("#latestpost").parent().find('img').attr 'src', _bburl

			$(".box.main .footer").addClass('needy').text("Swipe to Like").slideDown()
			T.serial.readM (_tagid)->
				console.log _tagid
				$('.box.main .footer').text "Got TagID, Processing..."
				$(T.ninja).trigger 'likePost'


		null
	null

T.getLatestPostShare = (_target)->
	console.log "Fetching latest post"
	_url = "http://tagbin.in/phpHandler/getDetails.php"
	_ops = {type: 'getLatestPost'}
	
	$.post _url, _ops, (response)->
		
		_object= response[0]
		_ll = T.Utils.NewPost _object

		
		
		$.post T.gdURL, {id: _id, type: 'getUserPicture'}, (_res)->
			_bb = T.b64toblob _res, 'image/jpg'
			_bburl = window.webkitURL.createObjectURL _bb
			$("#latestpost-share").parent().find('img').attr 'src', _bburl

			$(".box.main .footer").addClass('needy').text("Swipe to Share").slideDown()
			T.serial.readM (_tagid)->
				console.log _tagid
				$('.box.main .footer').text "Got TagID, Sharing..."
				$(T.ninja).trigger 'sharePost'


		null
	null




T.getImage  = (_url)->
	console.log _url
	_url = "http://tagbin.in/phpHandler/getDetails.php"
	_ops = {type: 'getLatestPost'}
	$.post _url, _ops, (response)->
		
	
T.updateLikes = (_target)->


T.get5LikePosts = (_target)->

	_TARGET = _target
	$(_TARGET).empty()
	$.post "http://tagbin.in/phpHandler/fb_getposts.php", {type: 'get5Posts'}, (responseStr)->
		$(_TARGET).addClass 'inProcess'
		i = 0
		response = JSON.parse(responseStr.toString())
		console.log response.data
		loop
			_obj = response.data[i]
			_li = T.Utils.NewPost _obj
			console.log _li
			_li.addClass('active') if response.data.indexOf( _obj ) is parseInt('2')
			$(_TARGET).append _li
			i++
			break if i is response.data.length
		$(_TARGET).removeClass 'inProcess'
		T.likePosts.next()
		T.likePosts.prev()
		T.likePosts.prev()
		T.likePosts.prev()




T.getHoroscope = (_action, _target)->
	T.checkTag (_tagid) ->
		console.log T.serial.tagID
	res1 = null
	data1 = null
	data2 = null
	data3 = null
	data4 = null
	arraydata = []
	sunsign = null
	
	getSunsign = (dob)->
		dob_mm= dob.slice(0,2)
		dob_mm = parseInt(dob_mm)
		dob_dd = dob.slice(3,5)
		dob_dd = parseInt(dob_dd)
		dob_yy= dob.slice(6,10)
		dob_yy = parseInt(dob_yy)
		switch dob_mm
			when 1 
				if 0 < dob_dd < 20 then "capricorn" 
				else "aquarius"
			when 2 
				if 0 < dob_dd < 19 then "aquarius" 
				else "pisces"
			when 3 
				if 0 < dob_dd < 21 then "pisces"
				else "aries"
			when 4 
				if 0 < dob_dd < 20 then "aries"
				else "taurus"
			when 5 
				if 0 < dob_dd < 21 then "taurus"
				else "gemini"
			when 6 
				if 0 < dob_dd < 21 then "gemini"
				else "cancer"
			when 7 
				if 0 < dob_dd < 23 then "cancer"
				else "leo"
			when 8 
				if 0 < dob_dd < 23 then "leo"
				else "virgo"
			when 9 
				if 0 < dob_dd < 23 then "virgo"
				else "libra"
			when 10 
				if 0 < dob_dd < 23 then "libra"
				else "scorpio"
			when 11 
				if 0 < dob_dd < 22 then "scorpio"
				else "sagittarius"
			when 12 
				if 0 < dob_dd < 22 then "sagittarius"
				else "capricorn"
	getSunsignImage = (sunsign) ->
		$("#sunsign").removeClass 'hide'
		switch sunsign
			when "aquarius" then $('#sunsign').attr 'src', "./static/img/fortune/aquarius.png"
			when "aries" then $("#sunsign").attr 'src',"./static/img/fortune/aries.png"
			when "cancer" then $('#sunsign').attr 'src',"./static/img/fortune/cancer.png"
			when "capricorn" then $("#sunsign").attr 'src' , "./static/img/fortune/capricorn.png"
			when "gemini" then $("#sunsign").attr 'src',"./static/img/fortune/gemini.png"
			when "libra" then $("#sunsign").attr 'src' , "./static/img/fortune/libra.png"
			when "leo" then $("#sunsign").attr 'src' , "./static/img/fortune/leo.png"
			when "pisces" then $("#sunsign").attr 'src' , "./static/img/fortune/pisces.png"
			when "sagittarius" then $("#sunsign").attr 'src' ,"./static/img/fortune/sagittarius.png"
			when "scorpio" then $("#sunsign").attr 'src',"./static/img/fortune/scorpio.png"
			when "taurus" then $("#sunsign").attr 'src' ,"./static/img/fortune/taurus.png"
			when "virgo" then $("#sunsign").attr 'src',"./static/img/fortune/virgo.png"			

	T.checkTag (_tagid) ->
		$(".box.main .footer").removeClass('pulsed').text "Loading Horoscope.."
		$.getJSON 'http://tagbin.in/my/ut/horoscope.php?tagid='+T.serial.tagID, (response)->
			$(".box.main .footer").text "Done"
			res1= response
			null
		.done (res1)->
			console.log res1   #res1=array(dob,name)
			_dob= res1[0]      #_dob = dob
			_name = res1[1]		# _name = user name
			_imgurl = res1[2]   # _img = user photo
			console.log(_dob)
			console.log(_name)
			console.log (_imgurl)
			sunsign= getSunsign(_dob)
			console.log sunsign    

			url= 'http://pipes.yahoo.com/pipes/pipe.run?_id=_omfgXdL3BGGadhGdrq02Q&_render=json&sign='+sunsign+'&url=http%3A%2F%2Ffeeds.astrology.com%2Fdailyoverview'
			$.getJSON url,(response)->
				data1 = response   #data1 = get wholehoroscope data
				data2 = data1.value.items[0].description  #data2= get only useful horoscope data 
				data2.search("</p>")
				data3 = data2.slice(3,data2.search("</p>"))
				arraydata = [data3 , _name , _imgurl] # arraydata = horotext , name
				null
			.done ()->
				console.log arraydata
				$("p#horousername").text("Welcome"+ " " + arraydata[1])
				$("p.horotext").text("Your Sun sign is"+"  " + sunsign)
				getSunsignImage(sunsign)	
				$("p.horo").text(arraydata[0])
#				$('.overlay').delay(8000).fadeIn()
				null
			null
		null
	null




T.getGraph = (_action, _target)->
    T.checkTag (_tagid) ->
        console.log T.serial.tagID
    a= null
    i = 0
    _results = []
    T.checkTag (_tagid) ->
        $.getJSON 'http://tagbin.in/my/ut/createleaderboard.php?tagid='+T.serial.tagID, (response)->
            a = response
            console.log a
            null
    
        .done ()->
                for i in [0..a[4][0].length-1]
                    _results.push({
                        name:a[4][0][i] ,
                        value:a[1][0][i]
                    });
                $("#chartContainer").dxChart({
    
                    dataSource: _results,
                    commonAxisSettings: {
                        grid: {
                            visible:false
                        },
                    },
                    valueAxis: {
                        label: {
                            visible:false
                        },
                    },
                    argumentAxis: {
                        label: {
                            visible:false
                        },
                        title:'Update Status or Upload Pics To Score Points On Leaderboard ',
                    },
                    legend: {
                        visible:false
                    },
                    commonSeriesSettings: {
                        type:'bar',
                        argumentField: 'name',
                    },
                    series: [{   
                        color: "#d1bff0",
                        label: {
                            isHtml:true,
                            visible: true,
                            backgroundColor:"#cc0063",
                            connector: {
                                visible: true
                            }
                            customizeText: ()->
                                if(parseInt(this.valueText)==a[3]||this.argumentText==a[2])
                                    return this.argumentText+": "+this.valueText+" points"
                        },
                        name:'leader',           
                        valueField: 'value'
                    }]
                    tooltip: {
                        enabled: true,
                        argumentFormat: 'MM/d',
                        customizeText: ()-> 
                            return this.argumentText + ' : ' + this.valueText+' points'
                        },  
                });
                null


T.getPoint = (_type,_tagid)->
    _url = "http://tagbin.in/my/ut/leaderboarddemo.php"
    $.post _url, {_type:_type,_tagid:_tagid},(res)->
        console.log res
        null
    null




T.likePosts = 
	
	liked: new Array()
	# Variables
	_url: 'http//tagbin.in/phpHandler/fb_likeposts.php'
	_container: '.posts-list'
	_target: null
	
	get: (_callback)->

		# Fetches posts from server to embed
		#$.getJSON T.likePosts.url , (response)->
		# Assume that posts are fetched correctly...scroll view to active element
		$.post "http://tagbin.in/phpHandler/getDetails.php", {type:'get5Posts'}, (response)->
			


#		T.likePosts.next()	# Assumes default active is 2nd elemnt
#		T.likePosts.prev()
		T.likePosts.prev()

	next: ()->
		target = $(".posts-list")
		_activePost = $(target).find(".social-post.active")
		
		actvP = $(_activePost)
		_aa = $(target).find('.social-post')
		if actvP[0] is _aa.eq(_aa.length-1)[0] then return
		nextP = $(_activePost).next()
		
		actvP
#			.find('.white').removeClass('thumb-color')
			.removeClass('active')
			.addClass('hidden')
			.removeClass('tiltdown')
			.addClass('tiltup')
		nextP
			.removeClass('hidden')
			.addClass('active')
			.parent()
			.stop()
			.animate({
				'scrollTop': actvP[0].offsetTop+100
			},300)

		target.find('.white').removeClass('thumb-color')
		target.find('.active').find('.white').addClass('thumb-color')

		return console.log "Next Post"

	prev: ()->
		target = $(".posts-list")
		_activePost = $(target).find(".social-post.active")
		
		actvP = $(_activePost)
		_aa = $(target).find('.social-post')
		if actvP[0] is _aa.eq(0)[0] then return
		prevP = $(_activePost).prev()

		actvP
			.removeClass('active')
			.addClass('hidden')
			.removeClass('tiltup')
			.addClass('tiltdown')
		prevP
			.removeClass('hidden')
			.addClass('active')
			.parent()
			.stop()
			.animate({
				'scrollTop': actvP[0].offsetTop-395
			},300)

		target.find('.white').removeClass('thumb-color')
		target.find('.active').find('.white').addClass('thumb-color')
		return console.log "Prev Post"

T.like_this = (_target) ->
	$('.active').find('.white').removeClass('thumb-color')
	T.actionlist.addStack 'Processing...', 'purple', 1
	$(T.ninja).trigger 'likePost'





T.vjblobs = {}

T.Displayimg = (url,id)->
	url64 = null
	window.webkitURL.revokeObjectURL T.vjblobs['user1']
	$.ajax({
		url: 'http://tagbin.in/phpHandler/linkedin/test.php',
		type: 'post',
		data:
			url1:url
		cache: false,
		success: (data)->
			url64 = JSON.parse data
			null
	}).done ()->
		b64 = url64
		t = T.b64toblob b64, 'image/jpeg'
		_bu = window.webkitURL.createObjectURL t
		console.log _bu
		T.vjblobs['user1'] = _bu
		$("img#" + id ).attr 'src', T.vjblobs['user1']
		null

T.getConnect = ()->

	$('#icom').removeClass().addClass('icomoon-connection')
	$('#icom_head').text 'Connect'
	user1 = null
	user2 = null
	user01 = null
	$.getJSON 'http://tagbin.in/phpHandler/linkedin/tagid1.php?tagid='+T.serial.tagID , (response)->
		user01= response
		user1 = JSON.parse user01[1]
		null
	.done () ->
		console.log user1
		if user1['pictureUrls']['_total']==1
			T.Displayimg(user1['pictureUrls']['values'][0],"tag-img1")
		$('.place_left1').attr 'class','place_left'
		$('.text1').text user1['firstName']		
		tmp = T.serial.tagID
		T.actionlist.addStack('Swipe to connect', '#BB2', 9)
		T.serial.readM (_tagid) ->
			$.getJSON 'http://tagbin.in/phpHandler/linkedin/tagid2.php?tagid='+T.serial.tagID, (res)->
				user2 = res;
				user2 = JSON.parse user2[1]
				$('.text2').text user2['firstName']
				if user2['pictureUrls']['_total']==1
					T.Displayimg(user2['pictureUrls']['values'][0],"tag-img2")
					console.log user2['pictureUrls']['values'][0]
				null
			.done () ->
				$('.place_right1').attr 'class','place_right'
				$.ajax({
					url: 'http://tagbin.in/phpHandler/linkedin/invitation.php',
					type: 'post',
					data:
						access_token: user01[0]
						lastName: user2['lastName']
						email: user2['emailAddress'] 
						firstName: user2['firstName']
						cache: false,
					success: (data)->
						if JSON.parse(data)['http_code']==201
							$('.box.main .footer').addClass('needy').text "Connect Request Sent"
						else
							$('.box.main .footer').addClass('needy').text "connection error"
						T.actionlist.addStack('Connected...', '', 9)					
						null
					}).done ()->
						console.log "Finished"
						$('.connectcheck1').attr 'class','connectcheck'	
						null
				null
			T.serial.tagID = tmp
			null
		null
	null




score = null
_a = null

T.getMatch = (_action, _target) ->
	T.checkTag (_tagid) ->
		console.log T.serial.tagID
	T.checkTag (_tagid) ->
		_url = 'http://tagbin.in/phpHandler/match_finder/match_finder.php'
		params = {tagid : T.serial.tagID}
		$.post _url, params, (response)->
			console.log response
			score = a = JSON.parse response
			$.post 'http://tagbin.in/phpHandler/getDetails.php', {type: 'getFbDetail', FbID : score[0].fbuserid}, (res) ->
				_a = JSON.parse res
				console.log _a
				$('#fr_name1').text _a.first_name + " " + _a.last_name
				$('#fr_username1').text _a.username
				$('#fr_score1').html score[0].score
				$('#fr_gender').html _a.gender
				if _a.location
					$('#fr_location').html _a.location.name
				$('#fr_hometown').html _a.hometown.name
				$('#fr_bio').html _a.bio
				$('.box-friend').removeClass 'hide'
				$.post 'http://tagbin.in/phpHandler/getDetails.php', {type:'getMutualFriends', tagID:T.serial.tagID, FbID2:score[0].fbuserid}, (response)->
					a = JSON.parse response
					_a = a = a.data
					$('#mf').html a.length + " Mutual Friends."
					$('#mf1').html a[0].name
					$('#mf2').html a[1].name
					$('#mf3').html a[2].name
					null
				$.post "http://tagbin.in/phpHandler/getDetails.php", {id: score[0].fbuserid, type: 'getUserPicture'}, (_res)->
					_bb = T.b64toblob _res, 'image/jpg'
					_bburl = window.webkitURL.createObjectURL _bb
					$("#fr_img").attr 'src', _bburl
				i = Object.keys score[1]
				if i.length > 0
					$.post 'http://tagbin.in/phpHandler/getDetails.php', {type: 'getName', ID : score[1][i[0]]}, (res) ->
						$('#like1').text res
					if i.length > 1
						$.post 'http://tagbin.in/phpHandler/getDetails.php', {type: 'getName', ID : score[1][i[1]]}, (res) ->
							$('#like2').text res
						if i.length > 2
							$.post 'http://tagbin.in/phpHandler/getDetails.php', {type: 'getName', ID : score[1][i[2]]}, (res) ->
								$('#like3').text res
				else $('#like1').text "No common likes"
				null
			null
		null




T.Music = 

	LIB: null
	Player: document.createElement('audio')

	getLib: (_callback)->
		
		$.getJSON "./music.json", (response)->
			T.Music.LIB = JSON.parse response
			console.log "music.json loaded"

			T.Music.Player.preload = true
			T.Music.Player.autoplay = true
			T.Music.Player.volume = 1
			if(_callback) then _callback()

	Populate: (_target)->

		$.ajax {
			url: 'music.json',
			cache: false,
			type: 'GET'
			dataType: 'json'

			error: (err)->
				console.log "EFETCH: Error fetching music.json, " + err
			success: (response)->
				# Player Config
				T.Music.Player.preload = true
				T.Music.Player.autoplay = true
				T.Music.Player.volume = 1
				T.Music.LIB = response

				$(_target)
					.find('.title')
					.text "PLAYLIST [" + response.length.toString() + " Songs]"
				i = 1 
				loop
					item = response.data[i]
					_li = $("<li/>")
								.addClass('song')
								.attr('data-id', i.toString())
								.text(item.title[0])

					
					# Append to ul
					$(_target)
						.find('ul')
						.append( _li )
					i++
					break if i is response.length+1
					
		}
		null

	onSongClick: (_songID, _target)->
		dd = T.Music.LIB.data[_songID]
		$(".player li.title").text dd.title[0]
		$(".player li.artist").text dd.artist[0]
		$(".player li.album").text dd.album[0]

		T.Music.getArt dd, _target
		T.Music.Play dd, _target

	Play: (dd, el)->

		# Visual Notification for currently playing music
		$(".play-list ul li.active").removeClass 'active'
		el.addClass 'active'

		T.Music.Player.src = dd.filePath
		console.log 'MUSIC: Playing '+ dd.title[0]

	getArt: (_object, el)->
		dd = _object
		#1_url = "https://www.googleapis.com/customsearch/v1?key=__key__&searchType=image&q=__query__&alt=__alt__&cx=__cx__&filetype=__filetype__"
		_url = "https://www.googleapis.com/customsearch/v1?key=__key__&searchType=image&q=__query__&alt=__alt__&cx=__cx__"
		_url = _url.replace('__key__', "AIzaSyCNBmZI7X9zOF-UMocaXCpaMEV-prsCWCY")
		_url = _url.replace('__alt__', "json")
		_url = _url.replace('__cx__', "015983474732296187492:hmmm431-awc")
		#1_url = _url.replace('__filetype__', "jpg")
		
		q2 = dd.album[0] ? dd.title[0]
		query = dd.artist[0] + " " + q2 + " album"
		_url = _url.replace '__query__', encodeURIComponent(query) 
		#console.log _url
		$.getJSON _url, (response)->
			_k = 0
			_finalLink = null
			loop
				_item = response.items[_k]
				_iM = _item.image
				console.log _iM.width, _iM.height, _iM.link
				if _iM.width>480 or _iM.height>480
					_finalLink = _item.link 
					console.log _item.link
				_k++
				break if _k is response.items.length
			console.log _finalLink
			$("img.player-art").attr('src', _finalLink)
			
		







T.navigate = (_target)->
	$.ajax({
		url: _target
		cache: false,
		success: (_res)->
		
			$(".app-content").hide "slide",{direction: 'down'}, 300, ()->
				$(".app-content").html _res
				$(".app-content").show "slide", {direction: 'up'}, 300

		
				# Full height of .box.app container
				$('.box.app').css('height', (document.height-50).toString() + 'px')

				console.log "Searching for .loadActions"
				
				# Search for actions available on load
				# console.log $(".loadActions").length
				$(".loadActions").each (_index)->
					T.actions $(@).attr('data-action'), $(@).attr('data-target')
					null
				null

			null

		error: (_res)->
			console.log "Error results"
			null

	}).done (_res)->
		console.log "Navigation Done, Applying View..."
		switch _target
			when 'like-post.html' then T.updateView 'entypo-thumbs-up', 'Like Posts', T.Utils.Colors.Purple
			when 'tagplay.html' then T.updateView 'icomoon-music', 'Tag Play', T.Utils.Colors.Orange
			when 'share.html' then T.updateView 'entypo-export', 'Share Posts', T.Utils.Colors.Red
			when 'status-post.html' then T.updateView 'entypo-vcard', 'Status Post', T.Utils.Colors.Blue
			when 'pic-upload.html' then T.updateView 'icomoon-image', 'Pic Upload', T.Utils.Colors.Green
			when 'feedback.html' then T.updateView 'icomoon-feed-2', 'Feedback', T.Utils.Colors.Brown
			when 'fortune.html' then T.updateView 'icomoon-dice', 'Horoscope', T.Utils.Colors.Vodared
			when 'leaderboard.html' then T.updateView 'icomoon-dashboard', 'LeaderBoard', T.Utils.Colors.Yellow
			when 'match_finder.html' then T.updateView 'icomoon-heart', 'FriendFinder', T.Utils.Colors.Pink
			when 'twitter-retweet.html' then T.updateView 'icomoon-users', 'Retweet', T.Utils.Colors.Dark_Green
			when 'connect.html' then T.updateView 'icomoon-connection', 'Connect', T.Utils.Colors.Color1
			else 'No Nav Switches'
		
		null
		

T.navActions = (_action)->
	if _action is 'navLikePost'
		T.actionlist.replace(['Swipe Tag', 'Like Post'])
		console.log "done-navlikepost"
	else console.log "Default Nav Action"
	null


T.updateView = (_iconclass, _viewname, _color)->
	T.Utils.CurrentColor = _color
	$(".main-page-title i")[0].className = "pageicon " + _iconclass
	$(".main-page-title span.name").text _viewname
	$(".main-page-title").show "drop", {direction: "left"}, 300
	$(".main-page-title").css 'color', _color
	$(".main-userpic-wrap .username").css 'background-color', _color
	$(".main-userpic-wrap .username").css 'border-bottom-color', _color





# Ninja Handlers here
$(T.ninja).on 'sendPhoto', (e)->
	_url = "http://tagbin.in/phpHandler/fb_post.php"
	_urlTemp = "http://tagbinphp-temp.herokuapp.com/phpHandler/fb_post.php"

	$.post _url, {
		imgdata: T.readyImageData
		tagID: T.serial.tagID
	}, (response)->
			$.post 'http://tagbin.in/phpHandler/store/store.php', {ID:response, type:'photo'}, (r) ->
				console.log r
			$('#capture').removeClass 'pulsed'
			$('#capture .text1').text "Done"
			T.getPoint 'photoupload', T.serial.tagID
			T.actionlist.addStack('Uploaded...', 'green', 2)
		null
	null


$(T.ninja).on 'likePost', (e)->
	_url = "http://tagbin.in/phpHandler/fb_likepost.php"
	_options = 
		postID: $('.posts-list .active').attr('data-post-id')
		tagID: T.serial.tagID
	$.post _url, _options, (_response)->
		console.log _response
		if _response.slice(_response.length-5, _response.length) != 'likes'
			T.actionlist.addStack("You're tagID is not registered or expired", 'purple', 1)
		else
			if _options.postID in T.likePosts.liked
				T.actionlist.addStack("You've already liked", 'purple', 1)
			else
				T.likePosts.liked.push _options.postID
				$('.posts-list .active .count').text(parseInt($('.posts-list .active .count').text())+1)
				T.actionlist.addStack('Liked...', 'purple', 1)
				T.getPoint 'like', T.serial.tagID
		null
	null


$(T.ninja).on 'sharePost', (e)->
	_url = "http://tagbin.in/phpHandler/share.php"
	_options = 
		postID: $('.posts-list .active').attr('data-post-id')
		tagID: T.serial.tagID
	$.post _url, _options, (_response)->
		console.log _response
		T.actionlist.addStack('shared...', 'red', 3)
		T.getPoint 'statusshare', T.serial.tagID
		null
	null





###
Notifications Module
Left Side, Below User-Picture
###

T.Notif = 

	# To store how many have been served
	Base: ['0']

	Create: ( message, type , callback)->
		target = $("ul.notif.actions")
		element = $("<li/>").addClass('action')

		# Apply Options
		element.text message
		element.addClass type

		# Append to list
		target.append element
		
		# Return Callback, if present
		callback() if typeof callback is 'function'

	isSuccess: ()->

	isError: ()->

	isInfo: ()->







T.picUpload = (_imgdata)->

	T.picReady = 1
	console.log "Ready to post the details"


	T.readyImageData = _imgdata
	$('#recapture').removeClass 'pulsed'
	$('#capture .text1').removeClass('pulsed').text 'Uploading...'
	T.actionlist.addStack 'Uploading...', 'green', 2
	$(T.ninja).trigger 'sendPhoto'

	null






T.serial =
	DEVICE_READY: false
	DEVICE_TIMEOUT: 500
	DEVICE_PORT: "/dev/ttyACM0"
	DEVICE_VENDORID: '2341'
	DEVICE_PRODUCTD: '0010'

	# Short Namespace for easy code
	S: chrome.serial

	interval:  null
	connInfo: null
	connId: null
	tagID: null

	isProcessing: false

	callback: null

	readM: (_cb)->
		if T.serial.isProcessing then window.clearInterval(T.serial.interval)
		T.serial.isProcessing = true
		T.serial.open()
		T.serial.callback = _cb
		null
	
	open: ()->
		###T.serial.S.getPorts (ports)->
			T.DEVICE_PORT = ports[0]
			console.log T.DEVICE_PORT, "Proceed to reading tag"
		###
		S = chrome.serial
		S.open T.serial.DEVICE_PORT, {bitrate: 9600}, T.serial.onOpen
		null

	onOpen: (connectionInfo)->
		T.serial.connInfo = connectionInfo
		T.serial.connId = T.serial.connInfo.connectionId
		if T.serial.connInfo.connectionId is -1 then console.log "System Permissions error on: "+ T.serial.DEVICE_PORT
		if typeof T.serial.connInfo.connectionId is 'number' then T.serial.listen()
		else console.error "Port cannot be opened, Some other error has occured"

	listen: ()->
		setInterval = setInterval || window.setInterval
		T.serial.interval = setInterval ()->
			T.serial.S.read T.serial.connId, 128, T.serial.onRead
		, T.serial.DEVICE_TIMEOUT
		null

	onRead: (readInfo)->
		if not T.serial.connId then return
		if readInfo and readInfo.bytesRead > 0 and readInfo.data then T.serial.processOutput readInfo.data
		else console.log "Swipe tag to read"

			
	processOutput: (_arrayBuffer)->
		str = T.serial._ab2str _arrayBuffer
		# Filter out the tag from junk data
		str = str.split('\n')[0].split(' ')[0]
		
		if str is '' or NaN or " " or "\n"
			console.log "Retrying..."
			$(T.ninja).trigger 'showRetry'
		
		if str.length.toString() is '22' or str.length.toString() is '24'
			T.serial.tagID = str
			T.serial.readSuccess()
		
		else 
			doNothing = 1
	
	readSuccess: ()->
		
		# Clear the repeating interval
		clearInterval = clearInterval || window.clearInterval
		clearInterval T.serial.interval

		# Close the serial port 	#!IMPORTANT
		T.serial.S.close T.serial.connId, (result)->
			console.log "Serial Port Closed"
			console.log "Value of tag from T.serial.tagID"
			T.serial.isProcessing = false
		# Return Value to Callback
		if typeof(T.serial.callback) is 'function' then T.serial.callback(T.serial.tagID)
		else console.log "No Callbacks, use T.readM(_callback) for reading tag with a callback"

	readTag: (readInfo)->
		str = T.serial._ab2str readInfo.data
		console.log "Tag read successful, tagID: " + str.toString()
		console.log T.serial.tagID + str
		tmp = T.serial.tagID + str
		T.serial.tagID = tmp.split('\n')[0].split(' ')[0]
		$(T.ninja).trigger 'gotTagID'
		T.serial.reset()
		null

	fullTagID: (readInfo)->
		if not T.serial.connId then return 
		if readInfo and readInfo.bytesRead > 0 and readInfo.data
			_data = T.serial._ab2str readInfo.data
			T.serial.tagID = _data.split('\n')[0].split(' ')[0]
			console.log "tagID: "+ T.serial.tagID
			$(T.ninja).trigger 'gotTagID'
		else
			console.log "Swipe tag to read"

	reset: ()->
		clearInterval = clearInterval || window.clearInterval
		clearInterval T.serial.interval
		T.serial.S.close T.serial.connId, (result)->
			console.log "Closed Serial Port"
		T.serial.open()
		null

	_ab2str: (buf)->
		return String.fromCharCode.apply null, new Uint8Array(buf)

T.checkTag = (_cbb)->
	if(T.serial.tagID)
		$('.success').text "You're logged-in"
		_cbb()
		return null
	else
		$('.success').text "Swipe your Tag"
		$('.username').text ""
		$('.userpic img').attr 'src', './static/img/default.png'
		T.serial.readM ()->
			$('.swipe_notification').removeClass('pulsed').text "Click apps to Access"
			$('.success').text "You're logged-in"
			$('.loader-menu').removeClass('_blur')
			$('.blur').addClass('hide')
			$.post 'http://tagbin.in/phpHandler/getDetails.php', {type:'getUserName', tagID: T.serial.tagID}, (res)->
				a = JSON.parse res
				$('.username').text a.firstname + " " + a.lastname
				null
			$.post "http://tagbin.in/phpHandler/getDetails.php", {tagID: T.serial.tagID, type: 'getUserDetails'}, (_res)->
				a = JSON.parse _res
#				_bb = T.b64toblob _res, 'image/jpg'
#				_bburl = window.webkitURL.createObjectURL _bb
#				$(".userpic img").attr 'src', _bburl
				T.getXHR "http://graph.facebook.com/" + a.fbuserid + "/picture?type=large", "blob", (responseBlob)->
					link = window.webkitURL.createObjectURL responseBlob
					$('.userpic img').attr 'src', link	#	css 'background-image', 'url("' + link + '")'
			_cbb()
			null
		null




T.share_this = (_target) ->
	T.actionlist.addStack 'Processing...', 'red', 3
	$(T.ninja).trigger 'sharePost'





T.activeSocketID = 0


T.sockets = ()->
	
	s = null

	### Initialize Chrome socket to read rfid tag event
	chrome.socket.create "tcp", {"bind": "127.0.0.1", "port": "5556"}, (result)->
		s = result if result
		console.log result
	###
	T.socket = chrome.socket

	onSocketCreateSuccess = (socketInfo)->
		socketId = socketInfo.socketId
		T.activeSocketID = socketId
		address = '127.0.0.1'
		port = 6338

		T.socket.listen socketId, address, port, (result)->
			console.assert 0 is result	# Socket Failed
			T.socket.getInfo socketId, (info)->
				console.log 'tagMachine Listener on http://localhost:' + info.localPort
				# accept only one time
				T.socket.accept socketId, (acceptInfo)-> 
					console.assert 0 is acceptInfo.resultCode
					acceptedSocketId = acceptInfo.socketId
					console.log 'acceptedSocketID', acceptedSocketId
					T.socket.read acceptedSocketId, (readInfo)->
						T.aB2Str readInfo.data, (str)->
							nlen = str.toString().split("\n")
							T.gotID = nlen[nlen.length-1].slice(6)
							console.log 'readInfo.data', T.gotID
							$(T.ninja).trigger 'sendPhoto'
							$(".box.main .footer").text("Uploading your photo")
							null
						null 
					null
				null
			null
		null
				
	

	T.socket.create "tcp", {}, onSocketCreateSuccess


T.aB2Str = (buf, callback)->
	blob = new Blob([new Uint8Array(buf)])
	f = new FileReader()
	f.onload = (e)->
		callback e.target.result
	f.readAsText(blob)
	null

T.socketReset = ()->
	# Destroy the socket
	chrome.socket.destroy T.activeSocketID, (result)->
		console.log result
		console.log "Closed Socket"

	# Re-Create a socket
	console.log "Creating Socket..."
	T.sockets()




S = {
	tweets : null,
	clicked_tweet_id : null,
	response : null,
	err : null,
}

T.getTweet = (_taget) ->
	_url = "http://tagbin.in/phpHandler/twitter/getDetail.php"
	params = {type: 'getLatestIdByHashTag', hash_tag: 'priyankachopra', count: 100}
	T.actionlist.addStack 'Click on Tweet to reTweet', '#151', 6
	$.post( _url, params, (response) ->
		a = S.tweets = JSON.parse(response)
		clone_under = $("#tw_detail")
		to_clone = $(".blog")
		i=0
		for tweet in a
			clone = to_clone.clone()
			c = clone_under.append clone
			_from = tweet.user.name + ' @ ' +tweet.user.screen_name
			clone.find('.tw_from').text _from
			clone.find('.tw_content').text tweet.text
			clone.find('#retw_count').text tweet.retweet_count
			$('a.blog').last().attr 'id', i
			$.post 'http://tagbin.in/phpHandler/getDetails.php', {type:'getPictureSrc', url : tweet.user.profile_image_url}, (url_)->
				_bb = T.b64toblob url_, 'image/jpg'
				_bburl = window.webkitURL.createObjectURL _bb
				console.log "-->" + _bburl
				clone.find('.tw_user_img').attr 'src', _bburl
			i++
		$("#tw_detail").find(".blog")[0].innerHTML = ""
		$('.box.main .footer').addClass('pulsed').text "Swipe your Tag!"
		T.checkTag (_tagid)->
			console.log _tagid
			$('a').click ()->
				console.log this.id
				S.clicked_tweet_id = S.tweets[this.id].id_str
				T.actionlist.addStack "ReTweeting...", '#151', 6
				$(T.ninja).trigger 'retweet'
				null
			null
		null
		).done () ->
			console.log 'sarveshh'
			null
		null


T.getTweetList = (_target) ->
	_TARGET = _target
	$(_TARGET).empty()
	_url = "http://tagbin.in/phpHandler/twitter/getDetail.php"
	params = {type: 'getLatestIdByHashTag', hash_tag: 'priyankachopra', count: 20}
	$.post _url, params, (response) ->
		$(_TARGET).addClass 'inProcess'
		i = 0
		a = S.tweets = JSON.parse response
		console.log a
		loop
			_obj = a[i]
			_li = T.NewTweet _obj
			_li.addClass('active') if a.indexOf(_obj) is parseInt('2')
			$(_TARGET).append _li
			i++
			break if i is a.length
		$(_TARGET).removeClass 'inProcess'
		T.likePosts.next()
		T.likePosts.prev()
		T.likePosts.prev()
		T.likePosts.prev()

T.NewTweet = (_data) ->
	tweet = _data.text ? "No Tweet"
	from_name = _data.user.name + ' @ ' +_data.user.screen_name
	tweet_id = _data.id_str

	_pobj = $("<div/>")
	_pobj.addClass('social-post row-fluid')
		.append( $("<div/>").addClass('span3'))
		.append( $("<div/>").addClass('span9'))
		.attr 'data-post-id', tweet_id

	_pobj.find('.span3')
		.append $('<div/>').addClass('pic-whoposted').attr('data-user-id', tweet_id)

	_picurl = _data.user.profile_image_url

	T.getXHR _picurl, 'blob', (responseBlob) ->
		_ourl = window.webkitURL.createObjectURL responseBlob
		_pobj.find('.pic-whoposted').css 'background-image', 'url("'+_ourl+'")'
	_pobj.find('.span9')
		.append( $('<div/>').addClass('who-posted').text(from_name) )
		.append( $('<div/>').addClass('post-text') )
	_pobj.find('.post-text')
		.append $('<p/>').text(tweet)
	_pobj.addClass('hidden tiltup')
	return _pobj

T.tweet_this = (_action) ->
	T.actionlist.addStack 'Processing...', '#151', 6
	$(T.ninja).trigger 'retweet'

$(T.ninja).on 'retweet', (e)->
	console.log 'triggered'
	_url = "http://tagbin.in/phpHandler/twitter/getDetail.php"
	params = {type: 'reTweet', tagID : T.serial.tagID, tweetID:$('.social-post.active').attr('data-post-id')}
	$.post _url, params, (response) ->
		console.log response
		S.err = JSON.parse(response).errors
		if S.err then T.actionlist.addStack 'Already Tweeted', '', 6
		else T.actionlist.addStack "reTweeted", '', 6
	null

$(T.ninja).on 'getUserPicture', (e)->
	console.log 'pic-triggered'
	url = "http://tagbin.in/phpHandler/twitter/getDetail.php"
	$.post url, {type: 'getUserPicture'}, (_res)->
		_bb = T.b64toblob _res, 'image/jpg'
		_bburl = window.webkitURL.createObjectURL _bb
		console.log _bburl






# Various utilities used by the app are defined here
T.Utils =

	# To Return random coordinates
	RandCoord: ()->
		_x = Math.floor Math.random()*document.width
		_y = Math.floor Math.random()*document.height
		return [parseInt(_x), parseInt(_y)]

	# Returns distance between two points
	# 		Points are defined as {x:0, y:0}
	Distance: (p1,p2)->
		dx = p2.x - p1.x
		dy = p2.y - p1.y
		return Math.sqrt(dx*dx + dy*dy)

	# Retuns String with First Letter UpperCased
	toStringFU: (string)->
		return string[0].toUpperCase() + string.substr(1)

	# Returns a post object based on data
	NewPost: (_data)->

		### Structure of _data
			|-- likes
			|	-- count
			|	-- data[]
			|     -- id
			|     -- name
			|-- from 
			|	-- id 
			|	-- name
			|-- id
			|-- message

		###
		
		# End if post is not status
		return if _data.type is not 'status'

		# Proceed with values
		_data.likes = _data.likes ? {count:'0', data: null}
		try 
			likes_count = _data.likes.data.length
		catch err
			likes_count = 0
		likes_data = _data.likes.data ? null
		message = _data.message ? "No Message"
		if message.length > 240
			message = message.slice(0,240)+"..."
		from_name = _data.from.name ? "User"
		from_id = _data.from.id ? "No ID"
		post_id = _data.id 
		
		
		# Create Post Object
		_pobj = $("<div/>")
		_pobj.addClass("social-post row-fluid")
			.append( $("<div/>").addClass('span3')  )
			.append( $("<div/>").addClass('span9') )
			.attr 'data-post-id', post_id

		# Add inner fields
		_pobj.find('.span3')
			.append( $("<div/>").addClass('pic-whoposted').attr('data-user-id', from_id ) )

		# Add picture
		# console.log "Adding Picture: " + from_id
		
		_picurl = 'http://graph.facebook.com/'+from_id+'/picture?type=large'
		
		T.getXHR _picurl, "blob", (responseBlob)->
			_ourl = window.webkitURL.createObjectURL responseBlob
			_pobj.find('.pic-whoposted').css 'background-image', 'url("' + _ourl + '")'
		
		_pobj.find('.span9')
			.append( $("<div/>").addClass('who-posted').text(from_name) )
			.append( $("<div/>").addClass('post-text') )
			.append( $("<div/>").addClass('post-likes') )
		_pobj.find('.post-likes')
			.append(  $("<div/>").addClass('count').text( likes_count ) )
			.append( $("<div/>").addClass('text_like') )
			.addClass("runAction").attr('data-action', 'like_this')
#			.attr 'onclick', 'T.like_this(' + post_id + ')'
		_pobj.find('.post-likes .text_like').append( $("<i/>").addClass('entypo-thumbs-up white') )
		_pobj.find('.post-text')
			.append(  $("<p/>").text( T.Utils.toStringFU(message) ) )
		_pobj.addClass('hidden tiltup')

		# Add Pictures of users who liked the post
		_pobj.find('.span9').append( $("<ul/>").addClass("like-users") )
		
		_k = 0
		return _pobj if not likes_data
		loop
			_tt = likes_data[_k]
			_ll = $("<li/>")
					.addClass('post-likes-pics')
					.attr('data-id', _tt.id)
					.attr('data-name', _tt.name)

			_likepicurl = 'http://graph.facebook.com/'+_tt.id+'/picture'
			_pobj.find("ul.like-users").append( _ll )
			_k++
			break if _k is likes_data.length or _k>6

		# Attach image to every member
		$(".post-likes-pics").each (index)->
			id = $(@).attr('data-id')
			url = "http://graph.facebook.com/" + id + "/picture"
			
			T.getXHR url, "blob", (responseBlob)->
				link = window.webkitURL.createObjectURL responseBlob
				selector = ".post-likes-pics[data-id='" + id + "']"
				console.log selector, $(selector).length
				$(selector).css 'background-image', 'url("' + link + '")'
		
		return _pobj

	# Color Variables
	Colors:
		Blue: '#33B5E5'
		Green: '#33DC49'
		Red: '#FF4444'
		Pink: '#ED3BA6'
		Orange: '#FFBB33'
		Yellow: '#f4b400'
		Purple: '#9d69b4'
		Dark_Green : '#151'
		Color1 : '#BB2'
		Color2 : '#CF0'
		Brown : '#D30'
		Vodared : '#ED1C24'

	CurrentColor: null




